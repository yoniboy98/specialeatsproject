<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>login/registration</title>
       <jsp:include page="jquery.jsp"></jsp:include>
      <script src="https://kit.fontawesome.com/c11c18cc81.js" crossorigin="anonymous"></script>
    <link href="FoodBank.css" rel="stylesheet" type="text/css">
</head>
<body>
 <jsp:include page="header.jsp"></jsp:include>
  
  <%
  String email = (String)session.getAttribute("email");
 //Wanneer de user al ingelogd is 
  if(email != null) {
	response.sendRedirect("Controller?action=LIST");  
  }
  
  String status = request.getParameter("status");
  if(status != null) {
	  if(status.equals("false")){
		  out.print("Bad credentials");
	  } else if (status.equals("error")){
		  out.print("some error occured ! ");
	  }
  }
  %>
  
  
  
  <div class="container">

<div class="login">
    <h3><span class="highlight">Purchase </span><span class="highlight2">your meal!</span></h3>
<br>
    <form action ="../loginprocess" method="post">

    <input type="email" name="email" id="email" class="form-control" placeholder="Enter email">
    <input type="password" name="password" id ="password" class="form-control" placeholder="Enter password">

    <button type="submit" name="loginBtn" class="btn">LOGIN</button>
    </form>
</div>
<div class="register">
<h2>REGISTER HERE</h2>

<div class="form-button">
    <form action = "../loginprocess" method="post">
    <input type="text" name="name" class="form-control" placeholder=" Enter firstname">
    
     <input type="text" name="familyname" class="form-control" placeholder="Enter lastname">

<input type="password" name="password" class="form-control" placeholder="Enter password">

<input type="email" name="email" class="form-control" placeholder="Enter email">

<input type="text" name="address" class="form-control" placeholder="Enter address">

<input type="number" name="number" class="form-control" placeholder="Enter number">
<button type="submit" name="registerBtn" class="btn">SIGN UP</button>
</form>
</div>


    </div>
</div>


<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>