<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="FoodBank.css" rel="stylesheet" type="text/css">
     <script src="https://kit.fontawesome.com/c11c18cc81.js" crossorigin="anonymous"></script>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     
<title>About us</title>
   <jsp:include page="jquery.jsp"></jsp:include>
</head>
<body>
   <jsp:include page="header.jsp"></jsp:include>
   <div class="about-section">
  <h1>About us</h1>
  <p>
Our mission is to deliver quality products to the customer with on top of that a meal that you have never tasted before.</p>
  <p>
Not only deliver but you can also pick up your food from us. (Click on the link for more information) <a href="contact.jsp">contact us</a></p>
</div>

<h2 style="text-align:center">Our Team</h2>
<div class="row">
  <div class="column">
    <div class="card">
      <img src="../images/bobross.jpg" alt="Yoni" style="width:25%">
      <div class="container">
        <h2>Yoni Vindelinckx</h2>
        <p class="title">Creator</p>
        <p>Yoni Vindelinckx is the creator of the SpecialEats website</p>
        <p>Yoni.Vindelinckx@hotmail.com</p>
    <a href="contact.jsp" class="button">Contact </a>
      </div>
    
  </div>

  <div class="column">
    <div class="card">
      <img src="../images/bobross.jpg" alt="Bob Ross" style="width:25%">
      <div class="containerabout">
        <h2>Bob Ross</h2>
        <p class="title">Art Director</p>
        <p>Bob Ross is a cool guy</p>
        <p>bob.rosse@example.com</p>
   <a href="contact.jsp" class="button">Contact </a>
      </div>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
      <img src="../images/bill.jpg" alt="Yoni" style="width:25%">
      <div class="container">
        <h2>Yoni Vindelinckx</h2>
        <p class="title">Designer</p>
        <p>Yoni Vindelinckx is the designer of the SpecialEats website</p>
        <p>Yoni.Vindelinckx@hotmail.com</p>
 <a href="contact.jsp" class="button">Contact </a>
      </div>
      </div>
    </div>
  </div>
   </div>
  <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>