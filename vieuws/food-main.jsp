<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/c11c18cc81.js" crossorigin="anonymous"></script>
   <link href="FoodBank.css" rel="stylesheet" type="text/css">
   
   <jsp:include page="jquery.jsp"></jsp:include>



    <title>Home page</title>
   
</head>

<body>
   <jsp:include page="header.jsp"></jsp:include>
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active" style="background-image: url(../images/thumb.jpg)">
      </div>

      <div class="item" style="background-image: url(../images/pizza.jpg)" >
      </div>
    
      <div class="item" style="background-image: url(../images/party.jpg)">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


   <div class="search">
   <form>
  <i class="fas fa-search "> </i>
<input type="text"  maxlength="2048" name="search" aria-haspopup="false"  autocomplete="off"
  spellcheck=false placeholder="What do you want to eat today?">
  </form>
 </div>
             

  <div class="rowroadmap">
    <div class="col-sm">
    <img src="../images/mobile.png" alt="stappenplan bestelling"> 
    <p class="de">Search for the product that you feel like in our extensive menu.</p>
        </div>
    <div class="col-sm"> 
   <img src="../images/prepared.png " alt="stappenplan bestelling"> 
     <p class="de">Your order will be prepared as quickly as possible.</p>
    </div>
    <div class="col-sm">
   <img src="../images/onderweg.png" alt="stappenplan bestelling"> 
      <p class="de">Our driver is on his way to your address in a hurry!</p>
    </div>
</div>
  


 <jsp:include page="footer.jsp"></jsp:include>

</body>
</html>