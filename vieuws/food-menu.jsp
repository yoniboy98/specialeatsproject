<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/c11c18cc81.js" crossorigin="anonymous"></script>
    <title>menu</title>
       <jsp:include page="jquery.jsp"></jsp:include>
    <link href="FoodBank.css" rel="stylesheet" type="text/css">
</head>
<body>

 <jsp:include page="header.jsp"></jsp:include>
  <div class="menutitle">
  <br>
      <h5> All this food is fresh and just prepared in our extensive kitchen with only top chefs on board!
            <span class="highlight2">enjoy your </span> <span class="highlight">meal!</span> </h5>
  </div>
 <div class ="gallery">
        <a target="_blank" href="chicken.png">
                <img src="../images/chicken.png" alt="chicken" width="600" height="400"> </a>
              <div class="desc">${food.name}  | prijs: ${food.price}<br>
             kcal: ${food.kcal}  | duration: ${food.duration}  <br><br>
             <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button>              
                 <button type="submit" class="btn btn-danger">Delete from card</button>  <i class="fas fa-trash-alt"></i>        
              </div>
             
    <jsp:include page="starRating.jsp"></jsp:include>

            </div>            
            <div class="gallery">
              <a target="_blank" href="gourmet.jpg">
                <img src="../images/gourmet.jpg" alt="gourmet" width="600" height="400"> </a>
              <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration}  <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
            </div>
                <jsp:include page="starRating.jsp"></jsp:include>
            
             </div>
            <div class="gallery">
              <a target="_blank" href="food.jpg">
                <img src="../images/food.jpg" alt="food" width="600" height="400"> </a>       
              <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration}<br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
               </div>
                   <jsp:include page="starRating.jsp"></jsp:include>
               
            </div>
            
            <div class="gallery">
              <a target="_blank" href="strawberry.jpg">
                <img src="../images/strawberry.jpg" alt="strawberry" width="600" height="400"></a>
              <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                  </div>
                      <jsp:include page="starRating.jsp"></jsp:include>
                  
            </div>
            <div class="gallery">
                    <a target="_blank" href="hamburger.jpg">
                      <img src="../images/hamburger.jpg" alt="hamburger" width="600" height="400"></a>
                    <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration}<br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button>
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                    </div>
                        <jsp:include page="starRating.jsp"></jsp:include>
                    
                  </div>
                  <div class="gallery">
                        <a target="_blank" href="kebab.jpg">
                          <img src="../images/kebab.jpg" alt="kebab" width="600" height="400"></a>
                        <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button>
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                   </div>
                       <jsp:include page="starRating.jsp"></jsp:include>
                   
                      </div>
                      <div class="gallery">
                            <a target="_blank" href="New.jpg.gif">
                              <img src="../images/New.jpg.gif" alt="new" width="600" height="400"></a>
                            <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                  </div>
                      <jsp:include page="starRating.jsp"></jsp:include>
                  
                          </div>
                          <div class="gallery">
                                <a target="_blank" href="some.jpg">
                                  <img src="../images/some.jpg" alt="some" width="600" height="400"></a>
                                <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                  </div>
                      <jsp:include page="starRating.jsp"></jsp:include>
                  
                              </div>
                              <div class="gallery">
                                    <a target="_blank" href="yangchowfried.jpg">
                                      <img src="../images/yangchowfried.jpg" alt="yangchowfried" width="600" height="400"></a>
                                    <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button> 
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
                  </div>
                      <jsp:include page="starRating.jsp"></jsp:include>
                  
                                  </div>
                                  <div class="gallery">
                                        <a target="_blank" href="strawberry.jpg">
                                          <img src="../images/strawberry.jpg" alt="strawberry" width="600" height="400"></a>
                                        <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="button" class="btn btn-primary">Add to card </button>
                 <button type="submit" class="btn btn-danger">Delete from card</button> <i class="fas fa-trash-alt"></i> 
               </div>
                   <jsp:include page="starRating.jsp"></jsp:include>
               
                </div>
                                      <div class="gallery">
                                            <a target="_blank" href="strawberry.jpg">
                                              <img src="../images/strawberry.jpg" alt="strawberry" width="600" height="400"></a>
                                            <div class="desc">${food.name}  | prijs: ${food.price}<br>
            kcal: ${food.kcal}  | duration: ${food.duration} <br><br>
               <i class="fas fa-cart-plus"></i>
             <button type="submit" class="btn btn-primary">Add to card </button>
             <button type="submit" class="btn btn-danger">Delete from card</button>      <i class="fas fa-trash-alt"></i> 
 </div>
     <jsp:include page="starRating.jsp"></jsp:include>
 
   </div>
 <script>
 var $star_rating = $('.star-rating .fa');

 var SetRatingStar = function() {
   return $star_rating.each(function() {
     if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
       return $(this).removeClass('fa-star-o').addClass('fa-star');
     } else {
       return $(this).removeClass('fa-star').addClass('fa-star-o');
     }
   });
 };

 $star_rating.on('click', function() {
   $star_rating.siblings('input.rating-value').val($(this).data('rating'));
   return SetRatingStar();
 });

 SetRatingStar();
 $(document).ready(function() {

 });
 </script>
 
 
</body>

<jsp:include page="footer.jsp"></jsp:include>

</html>