<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>orders</title>
       <jsp:include page="jquery.jsp"></jsp:include>
  
    <link href="vieuws/FoodBank.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
       <script src="https://kit.fontawesome.com/c11c18cc81.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  </head>
<body>
 <jsp:include page="header.jsp"></jsp:include>

 
 <div class="container">

     <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
   ${message}
  </div>
  
  <button id ="addbutton" class = "btn btn-warning" onclick="window.location.href='vieuws/food-add.jsp'">Add new food</button>

<div class="table-content">
 <table border ="1" class ="table table-striped table-bordered">

     <tr>
         <th> id</th>
         <th> name</th>
         <th> price</th>
         <th> kcal</th>
         <th> duration</th>
         <th> popularity</th>
         <th> Actions</th>
     </tr>
    
     <tr>
        <c:forEach items ="${list}" var ="food">
            <tr>
            <td>${food.id} </td>
            <td>${food.name} </td>
            <td>€ ${food.price} x1  </td>
            <td>${food.kcal} kcal/portie </td>
            <td>${food.duration} </td>
            <td>${food.popularity} </td>
            
            <td>
            <a href="${pageContext.request.contextPath}/Controller?action=EDIT&id=${food.id}"> Edit </a>
            |
            <a href="${pageContext.request.contextPath}/Controller?action=DELETE&id=${food.id}"> Delete </a>
            
            </td>
            </tr>
            </c:forEach>
 </table>
 </div>
  
  </div>    
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>