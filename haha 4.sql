DROP database foodbank;
CREATE DATABASE foodbank;
use foodbank;

CREATE TABLE foodBank (
	id INT,
	name varchar (50),
	price DOUBLE,
	kcal INT,
	duration time,
	population  varchar (25),
	PRIMARY KEY (`id`)
    
);
insert into foodBank values (1,'spaghetti', '15.00' , '480','20.00', 'very popular');
insert into foodBank values (2,'frietjes met stoofvlees', '5.00' , '790',15, 'popular');
insert into foodBank values (3,'hotdog', '3.50' , '280',2, 'very popular');
insert into foodBank values (4,'hamburger', '5.00' , '500',5, 'popular');
insert into foodBank values (5,'cheeseburger', '5.50' , '520',5, ' popular');
insert into foodBank values (6,'gerookte kip', '7.90' , '370',20, 'not popular');
insert into foodBank values (7,'zalm', '7.80' , '300',15, 'not popular');
insert into foodBank values (8,'clubsandwich', '8.90' , '250',3, 'very popular');
insert into foodBank values (9,'pizza margerita', '10.99' , '600',15, 'popular');
insert into foodBank values (10,'pizza salami', '10.99' , '620',15, 'very popular');
insert into foodBank values (11,'lasagne (klein)', '8.00' , '280',25, ' popular');
insert into foodBank values (12,'lasagne (groot)', '12.00' , '380',25, ' popular');
insert into foodBank values (13,'tacos', '4.80' , '250',20, 'very popular');
insert into foodBank values (14,'scampis', '15.00' , '310',20, 'not popular');
insert into foodBank values (15,'geitenkaas', '3.95' , '250',10, 'very popular');
insert into foodBank values (16,'pitta', '4.99' , '570',10, 'very popular');
insert into foodBank values (17,'krokketjes met sauce naar keuzen', '15.00' , '480',20, 'very popular');
insert into foodBank values (18,'special meal', '100.00' , '850',35, 'secret');

CREATE TABLE login (
	id INT,
	name VARCHAR (50),
	familyname VARCHAR (50),
	password  VARCHAR (50),
	email TEXT,
	locatie  VARCHAR (50),
	telenumber BIGINT,
	PRIMARY KEY (`id`)
);
insert into login values (1,'Yoni','Vindelinckx', 'geenpassword' , 'yoni.vindelinckx@hotmail.com','Halle', '0470565378');