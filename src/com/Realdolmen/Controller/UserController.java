package MyDbServlet2.src.com.Realdolmen.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import MyDbServlet2.src.com.Realdolmen.DAO.LoginDAOImpl;
import MyDbServlet2.src.com.Realdolmen.entity.UserRegister;


@WebServlet(name = "UserController", urlPatterns = "/loginprocess")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LoginDAOImpl logindao = null;

	public UserController() {
		logindao = new LoginDAOImpl();
	}

	HttpSession session;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		session = request.getSession();

		if (request.getParameter("loginBtn") != null) {
			startLogin(request, response);
		} else if (request.getParameter("registerBtn") != null) {
			startRegister(request, response);
		}
	}

	private void startRegister(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if(req.getParameter("name") != null && req.getParameter("familyname") != null && req.getParameter("password") != null
		&& req.getParameter("email") != null && req.getParameter("address") != null && req.getParameter("number") != null) {
			UserRegister ur = new UserRegister();
			ur.setName(req.getParameter("name"));
			ur.setFamilyname(req.getParameter("familyname"));
			ur.setPassword(req.getParameter("password"));
			ur.setEmail(req.getParameter("email"));
			ur.setAddress(req.getParameter("address"));
			ur.setnumber(req.getParameter("number"));
			
			if(logindao.saveForUser(ur)) {
				req.setAttribute("registermessage", "you have been successfully registered!");		
			} else {
				resp.sendRedirect("../errors.jsp");
			}
		}
	}

	private void startLogin(HttpServletRequest req, HttpServletResponse resp) {
		if (req.getParameter("password") != null && req.getParameter("email") != null) {

			UserRegister ur = new UserRegister();
			ur.setPassword(req.getParameter("password"));
			ur.setEmail(req.getParameter("email"));

			if (logindao.authenticate(ur)) {
				session.setAttribute("email", ur.getEmail());
				try {
					resp.sendRedirect("Controller?action=LIST");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.getMessage();
				}
			} else {
				try {
					resp.sendRedirect("vieuws/food-login.jsp?status=false");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.getMessage();
				}
			}
		}
	}
}