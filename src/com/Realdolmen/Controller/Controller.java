package MyDbServlet2.src.com.Realdolmen.Controller;


import java.io.IOException;
import java.sql.Time;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MyDbServlet2.src.com.Realdolmen.DAO.FoodDao;
import MyDbServlet2.src.com.Realdolmen.entity.Food;





@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RequestDispatcher dispatcher = null;
 FoodDao foodDao = new FoodDao();

  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
String action = request.getParameter("action");

if(action == null) {
	action = "LIST";
}

switch(action) {
case"LIST":
foodList(request, response);
break;
case "EDIT": 
	getSingleProductFromDB(request,response);
	break;
case "DELETE":
	deleteProduct(request,response);
	break;
default: foodList(request, response);
break;


}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		double price = Double.parseDouble(request.getParameter("price"));
		int kcal = Integer.parseInt(request.getParameter("kcal"));
		Time duration = Time.valueOf((request.getParameter("duration")));
		String popularity = request.getParameter("popularity");
			
	Food food = new Food();
	food.setId(Integer.parseInt(id));
		food.setName(name);
		food.setPrice(price);
		food.setKcal(kcal);
		food.setDuration(duration);
		food.setpopularity(popularity);
		
	
		  if(foodDao.save(food)) {
				 request.setAttribute("message","Save successfully!" );	 
	 } else {
		 if(foodDao.update(food)) {

			 request.setAttribute("message","Update successfully!" );
		 } 
	 }
	
	
foodList(request,response);

	}
	
	public void foodList(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		
		List<Food> list = foodDao.get();
		
		
		req.setAttribute("list", list);
			
		 dispatcher = req.getRequestDispatcher("/vieuws/food-list.jsp");
		dispatcher.forward(req, res);
	}
	
	
	public void getSingleProductFromDB(HttpServletRequest req , HttpServletResponse res) throws ServletException, IOException {
	
	String id = req.getParameter("id");
	Food food =foodDao.get(Integer.parseInt(id));
	req.setAttribute("food", food);
	dispatcher = req.getRequestDispatcher("/vieuws/food-add.jsp");

	dispatcher.forward(req, res);

 
}
	public void deleteProduct(HttpServletRequest req , HttpServletResponse res) throws ServletException, IOException {
		String id = req.getParameter("id");
		if(foodDao.delete(Integer.parseInt(id))) {
			req.setAttribute("message", "Product has been deleted!");
		}
		foodList(req,res);



}
}