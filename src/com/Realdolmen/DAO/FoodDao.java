package MyDbServlet2.src.com.Realdolmen.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import MyDbServlet2.src.com.Realdolmen.entity.Food;
import MyDbServlet2.src.com.Realdolmen.util.DBConnectionUtil;



public class FoodDao implements FoodBankDao {

	Connection connection = null;
	Statement statement = null;
	ResultSet resultSet = null;
	java.sql.PreparedStatement preparedstatement;

	@Override
	public List<Food> get() {

		// create refernce variabels
		List<Food> list = new ArrayList<>();
		Food food = null;

		try {
			// create a sql query
			String sql = "SELECT * FROM `foodbank`.`foodbank`;";
			// get db connection
			connection = DBConnectionUtil.openConnection();
			// create a statement
			statement = connection.createStatement();
			// execute the sql query
			resultSet = statement.executeQuery(sql);
			// process the resultset
			while (resultSet.next()) {
				food = new Food();
				food.setId(resultSet.getInt("id"));
				food.setName(resultSet.getString("name"));
				food.setPrice(resultSet.getDouble("price"));
				food.setKcal(resultSet.getInt("kcal"));
				food.setDuration(resultSet.getTime("duration"));
				food.setpopularity(resultSet.getString("popularity"));

				// add employee to list
				list.add(food);
			}
		} catch (Exception ex) {
			ex.getMessage();
		}
		// return the list
		return list;

	}

	@Override
	public boolean save(Food f) {

	boolean flag =false;
	try {
		String sql = "INSERT INTO foodbank (id, name, price, kcal, duration, popularity)VALUES('"+f.getId()+"', '"+f.getName()+"', '"+f.getPrice()+"','"+f.getKcal()+"','"+f.getDuration()+"','"+f.getpopularity()+"')";
	connection = DBConnectionUtil.openConnection();
	preparedstatement = connection.prepareStatement(sql);
	preparedstatement.executeUpdate();
	flag = true;
	} catch (SQLException ex) {
		ex.getMessage();	
	}
	return flag;
}

	@Override
	public Food get(int id) {
	
		Food food = null;
		try {
			 food = new Food();
			String sql ="SELECT * FROM foodbank WHERE id="+id;
			connection = DBConnectionUtil.openConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				food.setId(resultSet.getInt("id"));
				food.setName(resultSet.getString("name"));
				food.setPrice(resultSet.getDouble("price"));
				food.setKcal(resultSet.getInt("kcal"));
				food.setDuration(resultSet.getTime("duration"));
				food.setpopularity(resultSet.getString("popularity"));
				
			}
		}catch(Exception ex) {
			ex.getMessage();
		}
		return food;
		
		
	}

	@Override
	public boolean update(Food f) {
boolean flag =false;
try {
	String sql = "UPDATE foodbank SET id='"+f.getId()+"', name='"+f.getName()+"', price='"+f.getPrice()+"', kcal='"+f.getKcal()+"', duration='"+f.getDuration()+"', popularity='"+f.getpopularity()+"' where id="+f.getId();
	connection = DBConnectionUtil.openConnection();
	preparedstatement = connection.prepareStatement(sql);
	preparedstatement.executeUpdate();
	flag= true;
}catch(SQLException ex) {
	ex.getMessage();
}return flag;
	}

	@Override
	public boolean delete(int id) {
	boolean flag = false;
	try {
		String sql = "DELETE FROM foodbank WHERE id=" +id;
	connection=	DBConnectionUtil.openConnection();
	preparedstatement=connection.prepareStatement(sql);
	preparedstatement.executeUpdate();
	flag=true;
	
	}catch(SQLException ex) {
		ex.getMessage();
	}
		
	return flag;
}
}
