package MyDbServlet2.src.com.Realdolmen.DAO;

import java.util.List;

import MyDbServlet2.src.com.Realdolmen.entity.Food;



public interface FoodBankDao {
List<Food> get();

 boolean save(Food e);
 
 Food get(int id);
 
 boolean update(Food f);
 
 boolean delete(int id);
}
