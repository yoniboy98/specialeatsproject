package MyDbServlet2.src.com.Realdolmen.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnectionUtil {
//define db properties
	private static final String URL ="jdbc:mysql://localhost:3306/foodBank";
	private static final String DRIVER ="com.mysql.jdbc.Driver";
	private static final String USERNAME ="root";
	private static final String PASSWORD ="";
	private static Connection connection =null;
	//define the static method 
	
	public static Connection openConnection() {
		//check the connection
		if(connection != null) {
			return connection;
		}else {
		//load the driver
		try {
			Class.forName(DRIVER);
		// get the connection 
		connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
		}catch (Exception ex) {
			ex.getMessage();
		}
		//retrun the connection 
		return connection;
	
	}
	}
}