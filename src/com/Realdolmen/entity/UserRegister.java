package MyDbServlet2.src.com.Realdolmen.entity;

public class UserRegister {
private int id; 
private String name;
private String familyname;
private String password;
private String email;
private String address;
private String number;


public UserRegister(int id, String name, String familyname, String password, String email, String address,
		String number) {
	super();
	this.id = id;
	this.name = name;
	this.familyname = familyname;
	this.password = password;
	this.email = email;
	this.address = address;
	this.number = number;
}




public UserRegister() {
	// TODO Auto-generated constructor stub
}


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public String getFamilyname() {
	return familyname;
}


public void setFamilyname(String familyname) {
	this.familyname = familyname;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public String getAddress() {
	return address;
}


public void setAddress(String address) {
	this.address = address;
}


public String getnumber() {
	return number;
}


public void setnumber(String number) {
	this.number = number;
}


@Override
public String toString() {
	return "UserRegister [id=" + id + ", name=" + name + ", familyname=" + familyname + ", password=" + password
			+ ", email=" + email + ", address=" + address + ", phonenumber=" +number + "]";
}


}
