package MyDbServlet2.src.com.Realdolmen.entity;

import java.sql.Time;
public class Food {
	
	    private int id;
	    private String name;
	    private double price;
	    private int kcal;
	    private Time duration;
	    private String popularity;


	    public Food(int id, String name, double price, int kcal, Time duration, String popularity) {
	        this.id = id;
	        this.name = name;
	        this.price = price;
	        this.kcal = kcal;
	        this.duration = duration;
	        this.popularity = popularity;
	    }

	

		public Food() {
			// TODO Auto-generated constructor stub
		}



		public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public double getPrice() {
	        return price;
	    }

	    public void setPrice(double price) {
	        this.price = price;
	    }

	    public int getKcal() {
	        return kcal;
	    }

	    public void setKcal(int kcal) {
	        this.kcal = kcal;
	    }

	    public Time getDuration() {
	        return duration;
	    }

	    public void setDuration(Time duration) {
	        this.duration = duration;
	    }

	    public String getpopularity() {
	        return popularity;
	    }

	    public void setpopularity(String popularity) {
	        this.popularity = popularity;
	    }

		@Override
		public String toString() {
			return "Food [id=" + id + ", name=" + name + ", price=" + price + ", kcal=" + kcal + ", duration="
					+ duration + ", popularity=" + popularity + "]";
		}
	}


